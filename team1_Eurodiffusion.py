from tkinter import *
import tkinter as tk
import pygame
from tkinter import filedialog
from PIL import ImageTk, Image
pygame.mixer.init()

BACKGROUND_IMAGE1=r"C:\Users\91824\OneDrive\Pictures\Screenshots\Screenshot (9).png"
BACKGROUND_IMAGE2=r"C:\Users\91824\OneDrive\Pictures\Screenshots\Screenshot (11).png"


def print_result(i, result):
    output_text.insert(END, f'\nCase Number {i}\n')
    for r in result:
        output_text.insert(END, f'{r[0]} {r[1]}\n')

def process_input():
    pygame.mixer.music.load("button-3.mp3")
    pygame.mixer.music.play(loops=0)
    # Clear the output text widget
    output_text.delete("1.0", END)

    # Get the input text from the input text widget
    input_text = input_text_widget.get("1.0", END)

    # Split the input text into lines
    lines = input_text.strip().split("\n")

    # Pass the input and run the algorithm for each case
    case_number = 1
    i = 0
    while i < len(lines):
        country_count = int(lines[i])
        algorithm = Algorithm()
        i += 1
        for j in range(i, i + country_count):
            algorithm.add_country(lines[j])
        i += country_count
        result = algorithm.run()
        print_result(case_number, result)
        case_number += 1
def open_main_window():
    pygame.mixer.music.load("button-3.mp3")
    pygame.mixer.music.play(loops=0)
    # Create the main window
    root = Toplevel()
    root.title("INPUT")
    root.geometry("800x500")
    def clear_input():
        pygame.mixer.music.load("button-3.mp3")
        pygame.mixer.music.play(loops=0)
        input_text_widget.delete("1.0", END)
        output_text.delete("1.0",END)
    
    # Load the image file
    bg = ImageTk.PhotoImage(file=BACKGROUND_IMAGE2)

    # Create the canvas with the same size as the image
    canvas1 = tk.Canvas(root, width=bg.width(), height=bg.height())
    canvas1.pack(fill="both", expand=True)
    # Display the image on the canvas
    canvas1.create_image(0, 0, anchor="nw", image=bg)
    
    global input_text_widget
    input_text_widget = tk.Text(canvas1, width=30, height=10)
    input_text_widget.pack(side="left", padx=200, pady=40)
    
    global output_text
    output_text = tk.Text(canvas1, width=30, height=10)
    output_text.pack(side="right", padx=200, pady=40)

    
    
    # Create a button to process the input
    process_button = Button(canvas1, text="Execute",bg = "pink" ,font=("Arial", 20),command = process_input)
    process_button.pack(side=RIGHT, pady=10)

    # Create a button to clear the input
    clear_button = tk.Button(canvas1, text="Clear", bg="pink", font=("Arial", 20), command=clear_input)
    clear_button.pack(side=RIGHT, pady=20)
    
    root.mainloop()
# Create the input window
input_window = Tk()
input_window.title("EURO DIFFUSION")
input_window.geometry("800x500")
welcome_label = tk.Label(input_window, text="", font=("Arial", 25))
welcome_label.pack(padx=30, pady=30)
# Load the image file
bg = ImageTk.PhotoImage(file=BACKGROUND_IMAGE1)
# Get the width and height of the image
image_width = bg.width()
image_height = bg.height()
# Create the canvas with the same size as the image
canvas1 = Canvas(input_window, width=image_width//200, height=image_height//50)
canvas1.pack(fill="both", expand=True)
# Display the image on the canvas
canvas1.create_image(-50, -50, anchor=NW, image=bg)

# Create a frame for the buttons
button_frame = Frame(input_window)
button_frame.pack(padx = 10)
def f():
    pygame.mixer.music.load("button-3.mp3")
    pygame.mixer.music.play(loops=0)
    input_window.destroy()

    # Create a button to enter the program
enter_button = Button(button_frame, text="Enter", font=("Arial", 20), bg="gold",command = open_main_window)
enter_button.pack(side=LEFT, padx=50)

    # Create a button to exit the program
exit_button = Button(button_frame, text="Exit",font=("Arial", 20), bg="gold", command=f)
exit_button.pack(side=RIGHT,pady=10)

input_window.mainloop()
